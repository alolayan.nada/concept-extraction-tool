from PyPDF2 import PdfFileReader
from pathlib import Path

pdf=PdfFileReader('')
search_words=['will']
concepts_dict={'Goal':['will be','proposed system'],'principle':['believe','should']}

concepts=[]
dict={}

for page in pdf.pages:
    page_num=page['/StructParents']
    page_text=page.extractText()
    #for text extracted from pdf sentences are better split by "." and not split line "\n" but is rstrip neccassary?
    #regular expressions could be used re.split('\.\W+|\?\W+|\!\W+', page_text)
    sentences = page_text.rstrip().split(".") 
    for sentence in sentences:
        for k in concepts_dict.keys():
            if any(word in sentence for word in concepts_dict[k]):
                #solves the problem with \n and other extra chars
                sentence=' '.join(sentence.split())
                key1=concepts_dict[k]
                dict[k]=[sentence,page_num]
                print(dict[k])

print(dict)




