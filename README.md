# Concept Extractor and ArchiMate model(XML) generator tool

A sample tool to extract concepts using Spacy library based on our previous definition and entity mapping to generate ArchiMate model.
(Sentiment analysis)

https://www.opengroup.org/archimate-forum/archimate-overview

## Contributing

## License

[MIT](https://choosealicense.com/licenses/mit/)
